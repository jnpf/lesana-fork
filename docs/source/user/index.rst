####################
 User Documentation
####################

Documentation that is useful for everybody.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting_started_command_line
   settings
   search
   moving_data_between_collections

   derivatives
