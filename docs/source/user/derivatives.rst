******************
lesana derivatives
******************

Front-ends
==========

Collector
---------

Collector_ is a Gtk3 app to manage collection inventories throught yaml
files, which also works on GNU/Linux mobile devices.

.. _Collector: https://git.sr.ht/~fabrixxm/Collector

linkopedia
----------

linkopedia_ is a read-only web interface to Lesana collections.

.. _linkopedia: https://git.sr.ht/~fabrixxm/linkopedia
